﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineSchool.Web.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using OnlineSchool.Web.Areas.Identity.Services;
using OnlineSchool.Web.Data.Entities;
using OnlineSchool.Web.Repositories;

namespace OnlineSchool.Web
{
  public class Startup
  {
    private string _moviesApiKey = "7f65c9a1-fae3-4df7-ae08-ceb6a1ee4e12";

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      _moviesApiKey = Configuration["Movies:ServiceApiKey"];
      services.Configure<CookiePolicyOptions>(options =>
      {
        // This lambda determines whether user consent for non-essential cookies is needed for a given request.
        options.CheckConsentNeeded = context => true;
        options.MinimumSameSitePolicy = SameSiteMode.None;
      });

      services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(
          Configuration.GetConnectionString("DefaultConnection")));

      services.AddDefaultIdentity<User>(options =>
        {
          options.Password.RequireDigit = false;
          options.Password.RequiredLength = 3;
          options.Password.RequireNonAlphanumeric = false;
          options.Password.RequireUppercase = false;
          options.Password.RequireLowercase = false;
          options.SignIn.RequireConfirmedEmail = false;
        })
        .AddRoles<IdentityRole>()
        .AddEntityFrameworkStores<ApplicationDbContext>();
      ;

      services.TryAddScoped<FileUploadService>();
      services.TryAddScoped<ApplicationDbInitializer>();

      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

      services.AddTransient<ICategoryRepository, CategoryRepository>();
      services.AddTransient<ITariffRepositiry, TariffRepositiry>();
      services.AddTransient<IAccountRepository, AccountRepository>();
      services.AddTransient<IVideoRepository, VideoRepository>();
      services.AddTransient<ICourseRepository, CourseRepository>();
      services.AddTransient<ICommentRepository, CommentRepository>();
      services.AddTransient<IMarkRepository, MarkRepository>();
      services.AddTransient<IVideoConfigRepository, VideoConfigRepository>();
      services.AddTransient<IEmailSender, EmailSender>();
      
      services.Configure<AuthMessageSenderOptions>(Configuration);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                //app.UseExceptionHandler("/Error/Error404");
            }
            else
            {
               // app.UseExceptionHandler("/Error/Error404");
                app.UseHsts();
            }

            //var result = string.IsNullOrEmpty(_moviesApiKey) ? "Null" : "Not Null";
            //app.Run(async (context) =>
            //{
            //  await context.Response.WriteAsync($"Secret is {result}");
            //});
            app.UseHttpsRedirection();
      app.UseStaticFiles();
      app.UseCookiePolicy();

      app.UseAuthentication();
      new ApplicationDbInitializer().SeedAsync(app).GetAwaiter();
      app.UseMvc(routes =>
      {
        routes.MapRoute(
          name: "default",
          template: "{controller=Home}/{action=Index}/{id?}");
      });
    }
  }
}