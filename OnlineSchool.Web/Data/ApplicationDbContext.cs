﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OnlineSchool.Web.Data.Configurations;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data
{
  public class ApplicationDbContext : IdentityDbContext
  { 
    public DbSet<Category> Categories { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Status> Statuses { get; set; }
    public DbSet<Tariff> Tariffs { get; set; }
    public DbSet<Transaction> Transactions { get; set; }
    public DbSet<UserRating> UserRatings { get; set; }
    public DbSet<UserType> UserTypes { get; set; }
    public DbSet<Video> Videos { get; set; }
    public DbSet<Сourse> Courses { get; set; }
    public DbSet<Mark> Marks { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<VideoConfig> VideoConfigs { get; set; }
        


    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      builder.ApplyConfiguration(new CategoryConfiguration());
      builder.ApplyConfiguration(new UserConfiguration());
      builder.ApplyConfiguration(new AccountConfiguration());
      builder.ApplyConfiguration(new StatusConfiguration());
      builder.ApplyConfiguration(new TariffConfiguration());
      builder.ApplyConfiguration(new TransactionConfiguration());
      builder.ApplyConfiguration(new UserRatingConfiguration());
      builder.ApplyConfiguration(new UserTypeConfiguration());
      builder.ApplyConfiguration(new VideoConfiguration());
      builder.ApplyConfiguration(new СourseConfiguration());
      builder.ApplyConfiguration(new CommentConfiguration());
      builder.ApplyConfiguration(new MarkConfiguration());

      
      builder.Entity<Category>()
          .HasData(JsonConvert.DeserializeObject<Category[]>(File.ReadAllText("Seed/Categories.json")));

      builder.Entity<Сourse>()
        .HasData(JsonConvert.DeserializeObject<Сourse[]>(File.ReadAllText("Seed/Сourses.json")));

        builder.Entity<Video>()
            .HasData(JsonConvert.DeserializeObject<Video[]>(File.ReadAllText("Seed/Videos.json")));
    }
  }
}