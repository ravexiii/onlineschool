﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineSchool.Web.Data.Entities
{
 public class Video : Entity
  {
    public string Title { get; set; }
    public string Size { get; set; }
    public string Duration { get; set; }
    public string Description { get; set; }
    public string VidePath { get; set; }
    public string StatusId { get; set; }
    public Status Status { get; set; }
    public string UserId { get; set; }
    public User User { get; set; }
    public double Rating { get; set; }
    public DateTime DateCreate { get; set; }
    public int Views { get; set; }
    public string CategoryId { get; set; }
    public Category Category { get; set; }
    public string СourseId { get; set; }
    public Сourse Сourse { get; set; }

    public IEnumerable<Mark> Marks { get; set; }
    public IEnumerable<Comment> Comments { get; set; }

    }
}