﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OnlineSchool.Web.Data.Entities
{
  public class Transaction : Entity
  {
    public DateTime Date { get; set; }
    public string UserId { get; set; }
    public User User { get; set; }
    public double Amount { get; set; }
    public string Description { get; set; }
    public double BalanceAfter { get; set; }
    public double BalanceBefore { get; set; }

    public IEnumerable<User> Users { get; set; }
  }
}