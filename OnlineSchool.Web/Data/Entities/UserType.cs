﻿using System.Collections;
using System.Collections.Generic;

namespace OnlineSchool.Web.Data.Entities
{
 public class UserType : Entity
  {
    public string Name { get; set; }

    public IEnumerable<User> Users { get; set; }
  }
}