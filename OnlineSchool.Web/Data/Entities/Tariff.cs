﻿using System.Collections;
using System.Collections.Generic;

namespace OnlineSchool.Web.Data.Entities
{
 public class Tariff : Entity
  {
    public string Name { get; set; }
    public double Price { get; set; }
    public bool IsActual { get; set; }

    public IEnumerable<Account> Accounts { get; set; }
  }
}