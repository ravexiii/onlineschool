﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Data.Entities
{
    public class Comment : Entity
    {
        public DateTime Time { get; set; }
        public string Value { get; set; }
        
        public string UserId { get; set; }
        public User User { get; set; }
        
        public string VideoId { get; set; }
        public Video Video { get; set; }
    }
}
