﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Data.Entities
{
    public class Mark : Entity
    {
        public string UserId { get; set; }
        public User User { get; set; }

        public int Rating { get; set; }

        public string VideoId { get; set; }
        public Video Video { get; set; }
    }
}
