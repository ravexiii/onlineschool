﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace OnlineSchool.Web.Data.Entities
{
  public class Category : Entity
  {
    public string Name { get; set; }
    public string SubCategoryId { get; set; }
    public Category SubCategory { get; set; }
    public String ImagePath { get; set; }

    public IEnumerable<Category> SubCategories { get; set; }
    public IEnumerable<Video> Videos { get; set; }
    }
}