﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OnlineSchool.Web.Data.Entities
{
  public class Account : Entity
  {
    public string UserId { get; set; }
    public User User { get; set; }
    public double Balance { get; set; }
    public long AccountNumber { get; set; }
    public DateTime Deadline { get; set; }
    public String TariffId { get; set; }
    public Tariff Tariff { get; set; }

    public IEnumerable<User> Users { get; set; }
    public IEnumerable<Tariff> Tariffs{ get; set; }
  }
}