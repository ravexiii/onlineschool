﻿namespace OnlineSchool.Web.Data.Entities
{
 public class UserRating : Entity
  {
    public string UserId { get; set; }
    public User User { get; set; }
    public double Rating { get; set; }
    public long ViewsCount { get; set; }
  }
}