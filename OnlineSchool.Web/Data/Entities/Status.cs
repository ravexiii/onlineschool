﻿using System.Collections;
using System.Collections.Generic;

namespace OnlineSchool.Web.Data.Entities
{
  public class Status : Entity
  {
    public string Name { get; set; }

    public IEnumerable<Video> Videos { get; set; }
  }
}