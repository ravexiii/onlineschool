﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace OnlineSchool.Web.Data.Entities
{
  public class User : IdentityUser
  {
    public string Name { get; set; }
    public string ImagePath { get; set; }
    public string UserTypeId { get; set; }
    public UserType UserType { get; set; }

    //ссылки
    [NotMapped] public Account Account { get; set; }

    [NotMapped] public Tariff Tariff { get; set; }

    [NotMapped] public UserRating UserRating { get; set; }

    [NotMapped] public IEnumerable<Video> Videos { get; set; }

    [NotMapped] public IEnumerable<Tariff> Tariffs { get; set; }

    [NotMapped] public IEnumerable<Transaction> Transactions { get; set; }
  }
}