﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Data.Entities
{
    public class Сourse : Entity
    {
        public string Name { get; set; }

        public string CategoryId { get; set; }
        public Category Category { get; set; }

        public IEnumerable<Video> Videos { get; set; }
    }
}
