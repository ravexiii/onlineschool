﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Data.Entities
{
  public class VideoConfig :Entity
  {
    public int VideoMaxSize { get; set; }
    public String VideoFormat { get; set; }
  }
}
