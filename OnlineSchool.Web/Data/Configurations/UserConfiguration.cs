﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class UserConfiguration : IEntityTypeConfiguration<User>
  {
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder
        .HasMany(c => c.Transactions)
        .WithOne(b => b.User)
        .HasPrincipalKey(b => b.Id)
        .OnDelete(DeleteBehavior.Restrict);

        }
  }
}
