﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class UserRatingConfiguration : IEntityTypeConfiguration<UserRating>
  {
    public void Configure(EntityTypeBuilder<UserRating> builder)
    {

      builder
        .HasOne(a => a.User)
        .WithOne(b => b.UserRating)
        .HasForeignKey<UserRating>(b => b.UserId);
    }
  }
}
