﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class VideoConfiguration : IEntityTypeConfiguration<Video>
  {
    public void Configure(EntityTypeBuilder<Video> builder)
    {

      builder.HasOne(c => c.Status)
        .WithMany(c => c.Videos)
        .HasForeignKey(c => c.StatusId)
        .OnDelete(DeleteBehavior.SetNull);

      builder.HasOne(c => c.User)
        .WithMany(c => c.Videos)
        .HasForeignKey(c => c.StatusId)
        .OnDelete(DeleteBehavior.SetNull);

        builder.HasOne(c => c.Category)
            .WithMany(c => c.Videos)
            .HasForeignKey(c => c.CategoryId)
            .OnDelete(DeleteBehavior.SetNull);

        builder.HasOne(c => c.Сourse)
            .WithMany(c => c.Videos)
            .HasForeignKey(c => c.СourseId);

    

      builder.HasMany(v => v.Marks)
                .WithOne(v => v.Video)
                .HasPrincipalKey(v => v.Id)
                .OnDelete(DeleteBehavior.SetNull);
        }
  }
}
