﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class СourseConfiguration : IEntityTypeConfiguration<Сourse>
  {
        public void Configure(EntityTypeBuilder<Сourse> builder)
        {
            builder.HasMany(c => c.Videos)
            .WithOne(c => c.Сourse)
            .HasPrincipalKey(c => c.Id)
            .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
