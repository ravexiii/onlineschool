﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
  {
    public void Configure(EntityTypeBuilder<Transaction> builder)
    {
      builder
        .HasOne(c => c.User)
        .WithMany(b => b.Transactions)
        .HasForeignKey(b => b.UserId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}