﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class CategoryConfiguration : IEntityTypeConfiguration<Category>
  {
    public void Configure(EntityTypeBuilder<Category> builder)
    {
      /// Рекурсионная связь 
      builder
        .HasMany(c => c.SubCategories)
        .WithOne(c => c.SubCategory)
        .HasForeignKey(c => c.SubCategoryId)
        .OnDelete(DeleteBehavior.SetNull);

      builder
        .HasOne(c => c.SubCategory)
        .WithMany(c => c.SubCategories)
        .HasPrincipalKey(c => c.Id)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}
