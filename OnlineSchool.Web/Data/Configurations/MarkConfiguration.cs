﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Data.Configurations
{
    public class MarkConfiguration : IEntityTypeConfiguration<Mark>
    {
        
        public void Configure(EntityTypeBuilder<Mark> builder)
        {
            builder
                .HasOne(c => c.Video)
                .WithMany(c => c.Marks)
                .HasForeignKey(c => c.VideoId)
                .OnDelete(DeleteBehavior.SetNull);



        }
        
    }
}
