﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class AccountConfiguration : IEntityTypeConfiguration<Account>
  {
    public void Configure(EntityTypeBuilder<Account> builder)
    {
      builder
        .HasOne(a => a.User)
        .WithOne(b => b.Account)
        .HasForeignKey<Account>(b => b.UserId);

      builder
        .HasOne(c => c.Tariff)
        .WithMany(b => b.Accounts)
        .HasForeignKey(b => b.TariffId)
        .OnDelete(DeleteBehavior.SetNull);
    }
  }
}