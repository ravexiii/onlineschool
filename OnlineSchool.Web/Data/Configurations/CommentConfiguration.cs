﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class CommentConfiguration : IEntityTypeConfiguration<Comment>
  {
    public void Configure(EntityTypeBuilder<Comment> builder)
    {
      builder
        .HasOne(a => a.Video)
        .WithMany(b => b.Comments)
        .HasForeignKey(c => c.Id)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}