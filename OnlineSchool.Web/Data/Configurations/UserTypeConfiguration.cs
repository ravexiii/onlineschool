﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data.Configurations
{
  public class UserTypeConfiguration : IEntityTypeConfiguration<UserType>
  {
    public void Configure(EntityTypeBuilder<UserType> builder)
    {
      builder
        .HasMany(c => c.Users)
        .WithOne(b => b.UserType)
        .HasForeignKey(b => b.UserTypeId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}