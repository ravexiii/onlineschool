﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineSchool.Web.Data.Migrations
{
    public partial class addCategoriesInSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ImagePath", "Name", "SubCategoryId" },
                values: new object[] { "4", "img/photo-4.jpg", "Кассир", "1" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ImagePath", "Name", "SubCategoryId" },
                values: new object[] { "5", "img/photo-4.jpg", "Регистратор", "1" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "4");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "5");
        }
    }
}
