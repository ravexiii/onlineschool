﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineSchool.Web.Data.Migrations
{
    public partial class AddCourse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Videos_VideoId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_VideoId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "VideoId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "СourseId",
                table: "Videos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CategoryId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Courses_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Videos_СourseId",
                table: "Videos",
                column: "СourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_CategoryId",
                table: "Courses",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Courses_СourseId",
                table: "Videos",
                column: "СourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Courses_СourseId",
                table: "Videos");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropIndex(
                name: "IX_Videos_СourseId",
                table: "Videos");

            migrationBuilder.DropColumn(
                name: "СourseId",
                table: "Videos");

            migrationBuilder.AddColumn<string>(
                name: "VideoId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_VideoId",
                table: "AspNetUsers",
                column: "VideoId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Videos_VideoId",
                table: "AspNetUsers",
                column: "VideoId",
                principalTable: "Videos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
