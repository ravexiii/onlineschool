﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineSchool.Web.Data.Migrations
{
    public partial class FinishCreateMainPage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ImagePath", "Name", "SubCategoryId" },
                values: new object[] { "1", "img/photo-1.jpg", "Бухгалтерия", null });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ImagePath", "Name", "SubCategoryId" },
                values: new object[] { "2", "img/photo-2.jpg", "Программирование", null });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "ImagePath", "Name", "SubCategoryId" },
                values: new object[] { "3", "img/photo-3.jpg", "Кулинария", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "2");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "3");
        }
    }
}
