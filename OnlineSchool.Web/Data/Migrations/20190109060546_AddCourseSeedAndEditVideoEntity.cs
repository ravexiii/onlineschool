﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineSchool.Web.Data.Migrations
{
    public partial class AddCourseSeedAndEditVideoEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "5",
                column: "ImagePath",
                value: "img/photo-5.jpg");

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "CategoryId", "Name" },
                values: new object[] { "1", "1", "Яишница" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Courses",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: "5",
                column: "ImagePath",
                value: "img/photo-4.jpg");
        }
    }
}
