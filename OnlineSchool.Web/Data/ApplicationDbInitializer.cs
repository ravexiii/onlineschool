﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Data
{
  public class ApplicationDbInitializer
  {
    public async Task SeedAsync(IApplicationBuilder app)
    {
      using (var score = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        var userManager = score.ServiceProvider.GetRequiredService<UserManager<User>>();
        var roleManager = score.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

        if (!await roleManager.RoleExistsAsync("Admin"))
        {
          IdentityRole roleAdmin = new IdentityRole("Admin");
          await roleManager.CreateAsync(roleAdmin);
        }

        if (!await roleManager.RoleExistsAsync("User"))
        {
          IdentityRole roleUser = new IdentityRole("User");
          await roleManager.CreateAsync(roleUser);
        }

        if (!await roleManager.RoleExistsAsync("Employee"))
        {
          IdentityRole roleEmployee = new IdentityRole("Employee");
          await roleManager.CreateAsync(roleEmployee);
        }

        if (!await roleManager.RoleExistsAsync("Manager"))
        {
          IdentityRole roleManagerDb = new IdentityRole("Manager");
          await roleManager.CreateAsync(roleManagerDb);
        }
        
        User admin = await userManager.FindByNameAsync("Admin");
        if (admin == null)
        {
          var user = new User
          {
            UserName = "webonlineschool@gmail.com",
            Email = "webonlineschool@gmail.com",
            ImagePath = "images/admin.png"
          };

          var result = await userManager.CreateAsync(user, "Admin321321");
          if (result.Succeeded)
          {
            await userManager.AddToRoleAsync(user, "Admin");
          }
          
        }
      }
    }
  }
}
