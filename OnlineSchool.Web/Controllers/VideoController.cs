﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using OnlineSchool.Web.Areas.Identity.Services;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;
using OnlineSchool.Web.Repositories;
using OnlineSchool.Web.ViewModels;

namespace OnlineSchool.Web.Controllers
{
  [Authorize]
  public class VideoController : Controller
  {
    private readonly ApplicationDbContext _dbContext;
    private readonly ITariffRepositiry _tariffRepositiry;
    private readonly IAccountRepository _accountRepository;
    private readonly IVideoRepository _videoRepository;
    private readonly ICategoryRepository _categoryRepository;
    private readonly IVideoConfigRepository _videoConfigRepository;
    private readonly ICourseRepository _courseRepository;
    private readonly FileUploadService _fileUploadService;
    private readonly IHostingEnvironment _environment;
    private readonly UserManager<User> _userManager;
    private ICommentRepository _commentRepository;
    private IMarkRepository _markRepository;


    public VideoController(ApplicationDbContext dbContext, ITariffRepositiry tariffRepositiry,
      IAccountRepository accountRepository, IVideoRepository videoRepository,
      ICategoryRepository categoryRepository, IHostingEnvironment environment,
      IVideoConfigRepository videoConfigRepository,
      ICourseRepository courseRepository,
      FileUploadService fileUploadService,
      UserManager<User> userManager,
      ICommentRepository commentRepository,
      IMarkRepository markRepository)
    {
      _dbContext = dbContext;
      _tariffRepositiry = tariffRepositiry;
      _accountRepository = accountRepository;
      _categoryRepository = categoryRepository;
      _environment = environment;
      _fileUploadService = fileUploadService;
      _videoRepository = videoRepository;
      _courseRepository = courseRepository;
      _videoConfigRepository = videoConfigRepository;
      _userManager = userManager;
      _commentRepository = commentRepository;
      _markRepository = markRepository;
    }

    [HttpGet]
    public IActionResult AddVideo()
    {
      List<Category> categories = _categoryRepository.GetList().ToList();
      ViewBag.categories = categories;
      return View();
    }

    [HttpPost]
    public async Task<IActionResult> AddVideo(VideoViewModel model)
    {
      String messageError = "";

      VideoConfig config = await GetVideoConfig();
      if (config == null)
      {
        ViewBag.MeesageError = "Ошибка настройки видео обратитесь к поддержке!";
        return View(model);
      }


      String type = model.VideoFile.FileName;
      if (!type.ToLower().EndsWith(config.VideoFormat))
      {
        ViewBag.MeesageError = $"Не верный формат видео, необходим {config.VideoFormat}";
        return View(model);
      }

      var size = model.VideoFile.Length;
      if (size > config.VideoMaxSize)
      {
        ViewBag.MeesageError = $"Не верный размер видео,  необходим  до  {config.VideoMaxSize}";
        return View(model);
      }

      if (ModelState.IsValid)
      {
        String userId = _userManager.GetUserId(User);
        Video video = new Video();
        User user = await _userManager.FindByIdAsync(userId);
        var path = Path.Combine(_environment.WebRootPath, $"videos\\{model.CategoryId}\\{userId}");
        _fileUploadService.Upload(path, model.VideoFile.FileName, model.VideoFile);
        video.VidePath = $"/videos/{model.CategoryId}/{userId}/{model.VideoFile.FileName}";
        Console.WriteLine(video.VidePath);
        String duration = "60";//GetVideoDuration(video.VidePath);

        

       /* ShellFile so = ShellFile.FromFilePath(path);
        double nanoseconds;
        double.TryParse(so.Properties.System.Media.Duration.Value.ToString(),
          out nanoseconds);

        if (nanoseconds > 0)
        {
          double seconds = (nanoseconds * 0.0001) / 1000;
          Console.WriteLine(seconds.ToString());
          duration = seconds.ToString();
        }
        */
        
        
        video.User = user;
        video.UserId = userId;
        video.Title = model.Title;
        video.Description = model.Description;
        video.DateCreate = DateTime.Now;
        video.Duration = duration;
        video.Rating = 0;
        video.Views = 0;
        video.CategoryId = model.CategoryId;
        _videoRepository.Create(video);
        return RedirectToAction("VideoDetails", "Video", new {id = video.Id});
      }

      return View(model);
    }

    private string GetVideoDuration(string filePath)
    {
      using (var shell = ShellObject.FromParsingName(filePath))
      {
        IShellProperty prop = shell.Properties.System.Media.Duration;
        var t = (ulong) prop.ValueAsObject;
        return TimeSpan.FromTicks((long) t).ToString();
      }
    }

    public IActionResult GetListVideo()
    {
      var videos = _videoRepository.GetList();

      return View(videos);
    }

    public IActionResult VideoRemove(string id)
    {
      Video videos = _videoRepository.GetList().FirstOrDefault(x => x.Id == id);

      _videoRepository.Delete(videos);

      return RedirectToAction("GetListVideo", "Video");
    }

    public async Task<IActionResult> VideoDetails(string id)
    {
      List<Comment> comments = new List<Comment>();
      Video videos = await _videoRepository.GetList().FirstOrDefaultAsync(x => x.Id == id);
      try
      {
       comments = await _commentRepository.GetList().Where(x => x.VideoId == id).ToListAsync();
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        
      }
     

      if (comments!=null)
      {
        ViewBag.comments = comments;
      }
  
      return View(videos);
    }

    public IActionResult GetVideosByCategory(string id)
    {
      CategoriesWithVideosViewModel categoriesWithVideosViewModel = new CategoriesWithVideosViewModel()
      {
        MainCategoryName = _categoryRepository.GetList().FirstOrDefault(x => x.Id == id).Name,
        Videos = _videoRepository.GetList().Where(x => x.CategoryId == id).ToList(),
        Сourses = _courseRepository.GetList().Where(x => x.CategoryId == id).ToList(),
        SubCategories = _categoryRepository.GetList().Include(x => x.Videos).Where(x => x.SubCategoryId == id).ToList()
      };
      return View(categoriesWithVideosViewModel);
    }

    public IActionResult GetVideosByCourse(string id)
    {
      var course = _courseRepository.GetList().FirstOrDefault(x => x.Id == id);
      course.Videos = _videoRepository.GetList().Where(x => x.СourseId == id).ToList();

      return View(course.Videos);
    }

    private async Task<VideoConfig> GetVideoConfig()
    {
      return _videoConfigRepository.GetList().FirstOrDefault();
    }

    [HttpGet]
    public async Task<IActionResult> CreateComment(string value, string id)
    {
      string UserId = _userManager.GetUserId(User);
      Video video = _videoRepository.GetList().FirstOrDefault(x => x.Id == id);
      User user = await _userManager.FindByIdAsync(UserId);

      var comment = new Comment()
      {
        Time = DateTime.Now,
        UserId = UserId,
        User = user,
        VideoId = id,
        Value = value,
        Video = video
      };
      if (comment == null)
      {
        Console.WriteLine("null");
        throw new Exception("Error null comment obj");
      }

      _commentRepository.Create(comment);
      return RedirectToAction("VideoDetails", "Video", new {id});
    }


    [HttpGet]
    public async Task<IActionResult> CreateMark(int value, string id)
    {
      string UserId = _userManager.GetUserId(User);
      Video video = _videoRepository.GetList().FirstOrDefault(x => x.Id == id);
      User user = await _userManager.FindByIdAsync(UserId);

      Mark mark = new Mark()
      {
        Rating = value,
        VideoId = id,
        UserId = UserId,
        User = user,
        Video = video
      };

      _markRepository.Create(mark);
      return RedirectToAction("VideoDetails", "Video", new {id});
    }
  }
}