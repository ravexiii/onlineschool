﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OnlineSchool.Web.Areas.Identity.Services;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;
using OnlineSchool.Web.Models;
using OnlineSchool.Web.Repositories;
using OnlineSchool.Web.ViewModels;

namespace OnlineSchool.Web.Controllers
{
  public class HomeController : Controller
  {
    private readonly ApplicationDbContext _dbContext;
    private readonly ITariffRepositiry _tariffRepositiry;
    private readonly IAccountRepository _accountRepository;
    private readonly IVideoRepository _videoRepository;
    private readonly ICategoryRepository _categoryRepository;
    private readonly FileUploadService _fileUploadService;
    private readonly IHostingEnvironment _environment;

    public HomeController(ApplicationDbContext dbContext, ITariffRepositiry tariffRepositiry,
      IAccountRepository accountRepository, IVideoRepository videoRepository,
      ICategoryRepository categoryRepository, IHostingEnvironment environment,
      FileUploadService fileUploadService)
    {
      _dbContext = dbContext;
      _tariffRepositiry = tariffRepositiry;
      _accountRepository = accountRepository;
      _categoryRepository = categoryRepository;
      _environment = environment;
      _fileUploadService = fileUploadService;
      _videoRepository = videoRepository;
    }

    [Authorize]
    [HttpGet]
    public IActionResult GetCategory(string id)
    {
      Category category = _categoryRepository.GetList().FirstOrDefault(x => x.Id == id);

      return View(category);
    }


    public IActionResult Index()
    {
      IQueryable<Category> categories = GetCategoryList();
      return View(categories);
    }

    [Authorize]
    public IQueryable<Category> GetCategoryList()
    {
      IQueryable<Category> categories = _categoryRepository.GetList().Where(x => x.SubCategoryId == null);

      return categories;
    }

    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";

      return View();
    }

    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";

      return View();
    }

    [Authorize]
    public IActionResult Privacy()
    {
      return View();
    }

    public ActionResult NotFound()
    {
      return View();
    }
    
    public async Task<IActionResult> GetPay()
    {
            ViewBag.Message = "Добрый день, вы можете приобрести пакет услуг для просмотра курсов";
            return View("GetPay");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
  }
}