﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;

[assembly: HostingStartup(typeof(OnlineSchool.Web.Areas.Identity.IdentityHostingStartup))]

namespace OnlineSchool.Web.Areas.Identity
{
  public class IdentityHostingStartup : IHostingStartup
  {
    public void Configure(IWebHostBuilder builder)
    {
    }
  }
}