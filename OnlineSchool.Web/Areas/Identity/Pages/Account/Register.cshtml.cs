﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using OnlineSchool.Web.Areas.Identity.Services;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;
using OnlineSchool.Web.Repositories;

namespace OnlineSchool.Web.Areas.Identity.Pages.Account
{
  [AllowAnonymous]
  public class RegisterModel : PageModel
  {
    private readonly ApplicationDbContext _dbContext;
    private readonly SignInManager<User> _signInManager;
    private readonly UserManager<User> _userManager;
    private readonly ILogger<RegisterModel> _logger;
    private readonly IEmailSender _emailSender;
    private readonly IHostingEnvironment _environment;
    private readonly FileUploadService _fileUploadService;

    public RegisterModel(
      ApplicationDbContext dbContext,
      IAccountRepository accountRepository,
      ITariffRepositiry tariffRepositiry,
      IHostingEnvironment environment,
      FileUploadService fileUploadService,
      UserManager<User> userManager,
      SignInManager<User> signInManager,
      ILogger<RegisterModel> logger,
      IEmailSender emailSender)
    {
      _userManager = userManager;
      _signInManager = signInManager;
      _logger = logger;
      _emailSender = emailSender;
      _environment = environment;
      _fileUploadService = fileUploadService;
      _dbContext = dbContext;
    }

    [BindProperty] public InputModel Input { get; set; }

    public string ReturnUrl { get; set; }

    public class InputModel
    {
      [Required(ErrorMessage = "Обязательное имя")]
      [Display(Name = "Name")]
      public string Name { get; set; }

      [Required(ErrorMessage = "Обязательно аватарка")]
      [Display(Name = "Image")]
      public IFormFile ImageFile { get; set; }

      [Required(ErrorMessage = "Обязательно почта")]
      [EmailAddress]
      [Display(Name = "Email")]
      public string Email { get; set; }

      [Required(ErrorMessage = "Обязательно пароль")]
      [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.",/*The {0} must be at least {2} and at max {1} characters long.*/
        MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Password")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Confirm password")]
      [Compare("Password", ErrorMessage = "Пароль и пароль подтверждения не совпадают.")]
      public string ConfirmPassword { get; set; }
    }

    public void OnGet(string returnUrl = null)
    {
      ReturnUrl = returnUrl;
    }

    public async Task<IActionResult> OnPostAsync(string returnUrl = null)
    {
      returnUrl = returnUrl ?? Url.Content("~/");
      if (ModelState.IsValid)
      {
        var user = new User
        {
          UserName = Input.Email,
          Name = Input.Name,
          Email = Input.Email,
          UserTypeId = "71f4324f-2097-4859-b357-34d1d8edcabc"
        };
        
        var path = Path.Combine(_environment.WebRootPath, $"images\\Avatars\\{Input.Email}");
        _fileUploadService.Upload(path, Input.ImageFile.FileName, Input.ImageFile);
        user.ImagePath = $"images/Avatars/{Input.Email}/{Input.ImageFile.FileName}";

        var result = await _userManager.CreateAsync(user, Input.Password);
        if (result.Succeeded)
        {
          _logger.LogInformation("User created a new account with password.");

          await _userManager.AddToRoleAsync(user, "User");
          var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
          var callbackUrl = Url.Page(
            "/Account/ConfirmEmail",
            pageHandler: null,
            values: new {userId = user.Id, code = code},
            protocol: Request.Scheme);
        
          await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
             $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

          await _signInManager.SignInAsync(user, isPersistent: false);

          /* Создаем номер счета для нового пользователя 6ти значный */
          int accountNumber = await GenerateAccountNumber();

          var account = new Data.Entities.Account
          {
            UserId = user.Id,
            User = user,
            Balance = 0,
            AccountNumber = accountNumber,
            Deadline = DateTime.Now, //todo: необходимо установить значение с удаленным редактированием
            TariffId = null,
            Tariff = null
          };
          createAccount(account);
          return LocalRedirect(returnUrl);
        }

        foreach (var error in result.Errors)
        {
          ModelState.AddModelError(string.Empty, error.Description);
        }
      }

      // If we got this far, something failed, redisplay form
      return Page();
    }

    private void createAccount(Data.Entities.Account account)
    {
      //_accountRepository.Create(account);
      _dbContext.Accounts.Add(account);
      _dbContext.SaveChangesAsync().GetAwaiter();
    }

    public async Task<int> GenerateAccountNumber()
    {
      Random random = new Random();
      int number = random.Next(100000, 999999);

      Data.Entities.Account user = _dbContext.Accounts.FirstOrDefault(c => c.AccountNumber == number);
      if (user != null)
      {
        number = await GenerateAccountNumber();
      }

      return number;
    }
  }
}