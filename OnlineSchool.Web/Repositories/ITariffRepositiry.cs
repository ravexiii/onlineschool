﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Repositories
{
 public interface ITariffRepositiry : IRepository<Tariff>
  {

  }
}
