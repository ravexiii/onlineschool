﻿using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            All = context.Categories;
        }
    }
}
