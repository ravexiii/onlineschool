﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Repositories
{
  public class TariffRepositiry : Repository<Tariff>, ITariffRepositiry
  {
    public TariffRepositiry(ApplicationDbContext context) : base(context)
    {
      All = context.Tariffs;
    }
  }
}
