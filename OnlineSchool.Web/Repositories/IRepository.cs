﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Domen.Repositories
{
 public interface IRepository<T> where T : Entity
  {
    IQueryable<T> GetList();
    void Create(T item);
    void Update(T item);
    void Delete(T item);
    Task<T> Find(String id);
  }
}
