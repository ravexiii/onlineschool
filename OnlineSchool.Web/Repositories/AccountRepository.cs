﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Repositories
{
  public class AccountRepository : Repository<Account>, IAccountRepository
  {
    public AccountRepository(ApplicationDbContext context) : base(context)
    {
      All = context.Accounts;
    }

    public async Task<Account> FindEqual(int accountNumber)
    {
      Account entity = null;
      try
      {
        entity = await All.FirstAsync(e => e.AccountNumber == accountNumber);
      }
      catch (InvalidOperationException ex)
      {
        Console.WriteLine($"Не удалось найти сущность {nameof(entity)} по {accountNumber} {Environment.NewLine}{ex}");
      }

      return entity;
    }
  }
}

