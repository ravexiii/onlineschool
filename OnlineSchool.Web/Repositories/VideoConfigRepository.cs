﻿using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data;
using OnlineSchool.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.Repositories
{
  public class VideoConfigRepository : Repository<VideoConfig>, IVideoConfigRepository
  {
    public VideoConfigRepository(ApplicationDbContext context) : base(context)
    {
      All = context.VideoConfigs;
    }
  }
}