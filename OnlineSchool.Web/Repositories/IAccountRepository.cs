﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineSchool.Domen.Repositories;
using OnlineSchool.Web.Data.Entities;

namespace OnlineSchool.Web.Repositories
{
  public interface IAccountRepository : IRepository<Account>
  {
  
    Task<Account> FindEqual(int accountNumber);
  }
}