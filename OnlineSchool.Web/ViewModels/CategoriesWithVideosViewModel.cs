﻿using OnlineSchool.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSchool.Web.ViewModels
{
    public class CategoriesWithVideosViewModel
    {
        public string MainCategoryName { get; set; }

        public List<Video> Videos { get; set; }

        public List<Category> SubCategories { get; set; }

        public List<Сourse> Сourses { get; set; }
    }
}
